﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MoneyBook
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void btLogin_Click(object sender, EventArgs e)
        {
            fLogin f = new fLogin();
            DialogResult result =  f.ShowDialog();
            if(result == System.Windows.Forms.DialogResult.OK)
            {
                //로그인이 성공함
                //1.자료를 불러와서 표시 (목록)

                //2.입/출금 등록 버튼을 활성화
                btIN.Enabled = true;   
                btOut.Enabled = true;  
            }
            else
            {
                //로그인이 실패함.
                //1.현재 표시되는 목록 제거

                //2.입/출금 등록 버튼을 비활성
                btIN.Enabled = false;   //입금버튼 비활성
                btOut.Enabled = false;  //출금버튼 비활성
            }
        }

        private void btIN_Click(object sender, EventArgs e)
        {
            fIN f = new fIN();
            DialogResult result = f.ShowDialog();
            if(result == System.Windows.Forms.DialogResult.OK)
            {
                //입력데이터 확인
                DateTime 입력일 = f.dtDate.Value;
                string 분류 = f.tbType.Text;
                string 금액 = f.tbAmt.Text;
                string 비고 = f.tbMemo.Text;

                //데이터를 추가한다.
                //(미구현)
                    

                //목록 추가 
                ListViewItem lv = lv1.Items.Add(입력일.ToShortDateString());
                lv.SubItems.Add(분류); //분류
                lv.SubItems.Add(금액); //금액
                lv.SubItems.Add("");  //출금
                lv.SubItems.Add(비고); //비고

            }
        }

        private void btOut_Click(object sender, EventArgs e)
        {
            fOut f = new fOut();
            DialogResult result = f.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                //입력데이터 확인
                DateTime 입력일 = f.dtDate.Value;
                string 분류 = f.tbType.Text;
                string 금액 = f.tbAmt.Text;
                string 비고 = f.tbMemo.Text;

                //데이터를 추가한다.
                //(미구현)


                //목록 추가 
                ListViewItem lv = lv1.Items.Add(입력일.ToShortDateString());
                lv.SubItems.Add(분류); //분류
                lv.SubItems.Add("");  //입금
                lv.SubItems.Add(금액); //금액
                lv.SubItems.Add(비고); //비고 

            }
        }
    }
}
